#!/bin/bash

#change security group and the db names if required

#grabbing the latest snapshot that has the name davabase within
snapshot=$(aws rds describe-db-snapshots | jq -r '[.DBSnapshots[]  |  select (.DBInstanceIdentifier == "vazabase2")] | max_by(.SnapshotCreateTime) | .DBSnapshotIdentifier' | sed 's/"//g')
sgid="sg-65dd8112"
pubsub1="subnet-d0dfcc9b"
pubsub2="subnet-903f74af"

echo "$snapshot"

#restore RDS
aws cloudformation create-stack --stack-name "vazabase2" \
--template-body file://templates/restoredb.json \
--parameters \
ParameterKey=DBUsername,ParameterValue="root" \
ParameterKey=DBPassword,ParameterValue="petclinic" \
ParameterKey=DBAllocatedStorage,ParameterValue="5" \
ParameterKey=DBInstanceClass,ParameterValue="db.m3.medium" \
ParameterKey=MultiAZDatabase,ParameterValue="true" \
ParameterKey=WebServerSecurityGroup,ParameterValue=$sgid \
ParameterKey=DBInstanceName,ParameterValue="vazabase2" \
ParameterKey=Snapshotidentifier,ParameterValue="$snapshot" \
ParameterKey=PubSub1,ParameterValue=$pubsub1 \
ParameterKey=PubSub2,ParameterValue=$pubsub2

#aws rds restore-db-instance-from-db-snapshot \
#  --db-instance-identifier vazabase2 \
#  --db-snapshot-identifier































#until aws rds describe-db-instances | jq '.DBInstances[] | .DBInstanceStatus' | grep available
#do
#  sleep 5
#done

  #aws rds create-db-snapshot \
  #--db-instance-identifier mm1shbzi92sbwi6 \
  #--db-snapshot-identifier mydbsnapshot1

#ParameterKey=VPC,ParameterValue="vpc-702ddd0b"
#"DBSecurityGroup": {
#    "Type": "AWS::RDS::DBSecurityGroup",
#    "Properties": {
#        "GroupDescription": "Grant database access to web server",
#        "DBSecurityGroupIngress": {
#            "EC2SecurityGroupId": {
#                "Ref": "WebServerSecurityGroup"
#            }
#        },
#        "EC2VpcId" : { "Ref": "VPC" }
#    },
#    "Metadata": {
#        "AWS::CloudFormation::Designer": {
#            "id": "91dfe29c-f25c-4e94-843d-006472327384"
#        }
#    }
#},
#--tags Key=Name,Value="LAMP"
