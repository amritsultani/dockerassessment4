#!/bin/bash

#create an AMI that has docker already installed and started
subnetID="subnet-d0dfcc9b"

echo "STARTING basic EC2 instance"
output=$(aws ec2 run-instances --key-name vazkey --subnet-id ${subnetID} --image-id ami-97785bed --instance-type t2.micro --region us-east-1 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=DockerAMI}]')
INSTID=$(echo "$output" | jq '.Instances[].InstanceId' | sed 's/"//g')
HOSTIP=$(echo "$output" | jq '.Instances[].NetworkInterfaces[].PrivateIpAddress' | sed 's/"//g')

echo "Instance ID = $INSTID"
echo "Private IP = $HOSTIP"

echo "WAITING for EC2 instance to be ready"
until ssh ec2-user@$HOSTIP 'hostname'
do
	sleep 10
    echo -n '.'
done

# install docker onto the instance
ssh ec2-user@$HOSTIP 'sudo yum -y update && sudo yum -y install docker'
# keep docker on, start docker and allow ec2-user to have permissions for docker
ssh ec2-user@$HOSTIP 'sudo chkconfig docker on && sudo service docker start && sudo usermod -aG docker ec2-user'

echo "Create the new AMI for Docker"
output=$(aws ec2 create-image --instance-id $INSTID --name 'DockerAMI' --region us-east-1)
ImageID=$(echo "$output" | jq '.ImageId' | sed 's/"//g')
echo "Image ID = $ImageID"
until aws ec2 describe-images --image-ids $ImageID --region us-east-1 | jq '.Images[].State' | sed 's/"//g' | grep available
do
	sleep 10
    echo -n '.'
done

echo "Delete instance"
aws ec2 terminate-instances --instance-ids $INSTID --region us-east-1
