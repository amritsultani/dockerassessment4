#!/bin/bash

echo "Validating dashboard stack"

output1=$(aws cloudformation validate-template --template-body file://templates/createdashboard.yml)
result1=$(echo $?)
if [[ $result1 != 0 ]]
then
  echo "There was a validation error"
  exit 1

else

targetgrp=$(aws elbv2 describe-target-groups | jq '.TargetGroups[] | select(.TargetGroupName=="petclinic") | .TargetGroupArn' | cut -d':' -f6)

lbname="app/pcClu-Publi-X6PSMU6FDHGA/0538bd2d43df117d"
servicename="petclinic"
clustername=" pcCluster-ECSCluster-URZGSMDJ5FAV"

# add refs in the template so it will grab the right target group

aws cloudformation create-stack --stack-name "CWDashboard" \
  --template-body file://templates/createdashboard.yml \
# add parameters

fi
# aws cloudwatch put-dashboard --dashboard-name PCDAVEmonitor --dashboard-body
# insert json widget code
