#!/bin/bash

#NOT REQUIRED
vpcid="vpc-764c8c0d"

sgData=$(aws ec2 create-security-group --description "Security group for Dave Consul" --group-name DaveConsulSG --vpc-id $vpcid)
sgID=$(echo "$sgData" | jq '.GroupId' | sed 's/"//g')
echo "SG ID = $sgID"

#to be chnaged to dynamic later on so it is not fixed
vpcCIDR=10.0.0.0/16
echo $vpcCIDR

aws ec2 authorize-security-group-ingress --group-id $sgID --protocol tcp --port 22 --cidr $vpcCIDR
aws ec2 authorize-security-group-ingress --group-id $sgID --protocol tcp --port 8080 --cidr $vpcCIDR
aws ec2 authorize-security-group-ingress --group-id $sgID --protocol tcp --port 8400 --cidr $vpcCIDR
aws ec2 authorize-security-group-ingress --group-id $sgID --protocol tcp --port 8500 --cidr $vpcCIDR
aws ec2 authorize-security-group-ingress --group-id $sgID --protocol tcp --port 53 --cidr $vpcCIDR

subnetID="subnet-d0dfcc9b"
#make these dynamic if there is time
amiid="ami-5b649826"

output=$(aws ec2 run-instances --image-id $amiid --count 1 --instance-type t2.micro \
--key-name vazkey --subnet-id $subnetID --iam-instance-profile Name=s3Vault \
--security-group-ids $sgID --no-associate-public-ip-address --user-data file://user_data2.sh)


INSTID=$(echo "$output" | jq '.Instances[].InstanceId' | sed 's/"//g')
# HOSTIP=$(echo "$output" | jq '.Instances[].NetworkInterfaces[].PrivateIpAddress' | sed 's/"//g')

echo $INSTID
# tagging
aws ec2 create-tags --resources $INSTID --tags Key=Name,Value=Daveconsul
