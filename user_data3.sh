#!/bin/bash

# see createEKstack.sh for more detail

aws s3 cp s3://vazbucket/java8.rpm java8.rpm
rpm -Uvh java8.rpm

yum -y install \
https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.1.3.rpm \
https://artifacts.elastic.co/downloads/kibana/kibana-6.1.3-x86_64.rpm


echo "
server.host: 0.0.0.0
">>/etc/kibana/kibana.yml
#change server.host from localhost to 0.0.0.0 for kibana.yml
# sudo vim /etc/kibana/kibana.yml

echo "
network.host: 0.0.0.0
">>/etc/elasticsearch/elasticsearch.yml
#change network.host to 0.0.0.0 for elasticsearch.yml
# sudo vim /etc/elasticsearch/elasticsearch.yml

chkconfig elasticsearch on
chkconfig kibana on
service elasticsearch start
service kibana start
