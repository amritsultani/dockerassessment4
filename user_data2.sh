#!/bin/bash

# see createconsul.sh for more detail
while [[ -z $docker0 ]]
do
  # docker0=$(/sbin/ifconfig | grep -A 1 docker0 | grep inet | sed 's/^[ \t]*//'| cut -d' ' -f2 | sed 's/addr://g')
  #docker0 should be the private address of the consul instance
  docker0=10.0.2.56

done

echo "Docker interface: $docker0" 1>&2
# Add a comment to this line

docker run -d -p 8400:8400 -p 8500:8500 -p 53:53/udp -h consul --name daveconsul progrium/consul -server -advertise $docker0 -bootstrap
