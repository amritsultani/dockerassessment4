#!/bin/bash

#create a t2.large instance that has Elasticsearch and Kibana installed and running
vpcid="vpc-764c8c0d"

sgData=$(aws ec2 create-security-group --description "Security group for EK stack" --group-name EKSG --vpc-id $vpcid)

sgID=$(echo "$sgData" | jq '.GroupId' | sed 's/"//g')
echo "SG ID = $sgID"

#to be changed to dynamic later on so it is not fixed
vpcCIDR=10.0.0.0/16
echo $vpcCIDR
officeip=77.108.144.180/32

aws ec2 authorize-security-group-ingress --group-id $sgID --protocol all --port 0-65535 --cidr $vpcCIDR
aws ec2 authorize-security-group-ingress --group-id $sgID --protocol all --port 0-65535 --cidr $officeip
#created a new security that allows all access in all ports from VPC

subnetID="subnet-d0dfcc9b"

amiid="ami-97785bed"

# build the instance, calling on the script user_data3.sh to be run when instance is built
aws ec2 run-instances --image-id $amiid --count 1 --instance-type t2.large \
--key-name vazkey --subnet-id $subnetID --security-group-ids $sgID \
--iam-instance-profile Name=s3Vault --user-data file://user_data3.sh \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=DAVEKStack}]'
