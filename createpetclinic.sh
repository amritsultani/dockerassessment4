#!/bin/bash

# NOT REQUIRED
# FOR TESTING PETCLINIC AND LOGSTASH USING EC2
vpcid="vpc-764c8c0d"

sgData=$(aws ec2 create-security-group --description "Security group for PetClinic" --group-name PCDaveSG --vpc-id $vpcid)

sgID=$(echo "$sgData" | jq '.GroupId' | sed 's/"//g')
echo "SG ID = $sgID"

#to be changed to dynamic later on so it is not fixed
vpcCIDR=10.0.0.0/16
echo $vpcCIDR

aws ec2 authorize-security-group-ingress --group-id $sgID --protocol all --port 0-65535 --cidr $vpcCIDR

subnetID="subnet-d0dfcc9b"
amiid="ami-5b649826"
# using the ami built before that has Docker installed and started

aws ec2 run-instances --image-id $amiid --count 1 --instance-type t2.large \
--key-name vazkey --subnet-id $subnetID --security-group-ids $sgID \
--iam-instance-profile Name=s3Vault --user-data file://user_data.sh \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=DockerPetClinic}]'
# --no-associate-public-ip-address to be added after testing if used in real life
