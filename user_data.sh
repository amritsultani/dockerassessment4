#!/bin/bash

# see createpetclinic.sh for more detail

while [[ -z $docker0 ]]
do
  docker0=$(/sbin/ifconfig | grep -A 1 docker0 | grep inet | sed 's/^[ \t]*//'| cut -d' ' -f2 | sed 's/addr://g')
done

output=$(aws ecr get-login --no-include-email --region us-east-1)
$output

docker pull 109964479621.dkr.ecr.us-east-1.amazonaws.com/dav_pc:latest

docker run -d -v /var/run/docker.sock:/tmp/docker.sock --name registrator -h registrator gliderlabs/registrator:latest consul://$docker0:8500

docker run -itd --restart always --name petclinic -P 109964479621.dkr.ecr.us-east-1.amazonaws.com/dav_pc:latest
