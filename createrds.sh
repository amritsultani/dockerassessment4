#!/bin/bash

sgid="sg-65dd8112"
pubsub1="subnet-d0dfcc9b"
pubsub2="subnet-903f74af"

#create RDS
aws cloudformation create-stack --stack-name "vazabase2" \
--template-body file://templates/mysql.json \
--parameters \
ParameterKey=DBName,ParameterValue="petclinic" \
ParameterKey=DBUsername,ParameterValue="root" \
ParameterKey=DBPassword,ParameterValue="petclinic" \
ParameterKey=DBAllocatedStorage,ParameterValue="5" \
ParameterKey=DBInstanceClass,ParameterValue="db.m3.medium" \
ParameterKey=MultiAZDatabase,ParameterValue="true" \
ParameterKey=WebServerSecurityGroup,ParameterValue=$sgid \
ParameterKey=DBInstanceName,ParameterValue="vazabase2" \
ParameterKey=PubSub1,ParameterValue=$pubsub1 \
ParameterKey=PubSub2,ParameterValue=$pubsub2
