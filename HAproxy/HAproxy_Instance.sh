#!/bin/bash

output=$(aws ec2 run-instances --key-name vazkey --subnet-id subnet-d0dfcc9b \
--image-id ami-97785bed --instance-type t2.micro --region us-east-1 --security-group-ids sg-65dd8112 \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=HAproxy}]')

INSTID=$(echo "$output" | jq '.Instances[].InstanceId' | sed 's/"//g')

sleep 10

HOSTIP=$(aws ec2 describe-instances --instance-ids $INSTID | jq '.Reservations[].Instances[].PublicIpAddress' | sed 's/"//g')

echo "WAITING for EC2 instance to be ready"
until ssh ec2-user@$HOSTIP 'hostname'
do
	sleep 10
        echo -n '.'
done

echo "Instance ID = $INSTID"
echo "Private DNS = $HOSTIP"

echo "Installing necessary software"
ssh ec2-user@$HOSTIP 'sudo yum -y install haproxy && sudo service haproxy start'

echo "Copying template onto Haproxy"
scp /var/lib/jenkins/workspace/HAproxy_Config/HAproxy/haproxy.ctmpl ec2-user@$HOSTIP:/tmp/
ssh ec2-user@$HOSTIP "sudo mv /tmp/haproxy.ctmpl /etc/haproxy/ && sudo chown root:root /etc/haproxy/haproxy.ctmpl"

echo "Copying Consul service into init.d"
scp /var/lib/jenkins/workspace/HAproxy_Config/HAproxy/consul ec2-user@$HOSTIP:/tmp/
ssh ec2-user@$HOSTIP "sudo mv /tmp/consul /etc/init.d/ && sudo chown root:root /etc/init.d/consul"
ssh ec2-user@$HOSTIP "cd /etc/init.d/ && sudo chmod +x consul && sudo chkconfig --add consul"

echo "Download the consul template"
ssh ec2-user@$HOSTIP "cd /tmp && wget https://releases.hashicorp.com/consul-template/0.12.0/consul-template_0.12.0_linux_amd64.zip && unzip consul-template_0.12.0_linux_amd64.zip && sudo chmod a+x consul-template && sudo mv consul-template /usr/local/bin/consul-template"

echo "Starting initial run"
ssh ec2-user@$HOSTIP "sudo service consul start"
