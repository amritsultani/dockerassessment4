# DockerAssessment4

Team assessment 4 which involves creating an automated environment to run the petclinic application on docker containers inside a ELK monitored ec2 instance.
In this repository are the codes used to create the Docker image that has the Spring boot application PetClinic and logstash installed and started in a container, and the scripts for monitoring. There are other scripts which was only used purely for testing and some which were made unnecessary, but included anyway.

The shell scripts that are used for the final output are:
- createdashboard.sh for creating Cloudwatch dashboard for monitoring
- createEKstack.sh for building up ec2 instance with Elasticserch and Kibana
- user_data3.sh used in createEKstack.sh
- stopservice.sg to delete the service on fargate (i.e. the PetClinic containers)
- dockerfile/startPC_Log.sh for starting PetClinic and logstash in container for the Docker image

The shell scripts for future purposes are:
- restorerds.sh in case our RDS called vazabase2 ever gets destroyed
